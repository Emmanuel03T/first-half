﻿using System;

namespace Numeritos
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu = 0;
            bool validar, semaforo;
            Random valorGenerado = new Random();

    
            do
            {

                semaforo = false;
                Console.WriteLine("Bienvenidos al Cajero Automatico del Banco FDP INVERSMENTS ");
                Console.WriteLine("Por favor Elija que Desea Hacer:");
                Console.WriteLine("\n 1.Retiro" +
           "\n 2.Transacción." +
           "\n 3.Salir");
                Console.WriteLine();    
                do
                {
                    do
                    {
                        Console.Write("Elija Opción: ");
                        validar = int.TryParse(Console.ReadLine(), out menu);
                    } while (!validar);
                } while (menu < 1 || menu > 2);
                Console.WriteLine();

                if (menu != 3)
                {

                    switch (menu)
                    {

                        case 1:

                            Console.WriteLine("FDP INVERSMENTS");
                            Console.WriteLine("Inserte el Monto a Retirar");
                            int imput, N1, N2, N3, N4, N11, N22,N33;
                            imput = int.Parse(Console.ReadLine());
                            Console.WriteLine();


                            if (imput >= 20001)
                            {
                                Console.WriteLine("El Monto que ha Solicitado No Puede Ser Completado");
                            }
                            if ((imput > 1999) ^ (imput < 20001))
                            {
                                Console.WriteLine("Usted ha Solicitado el Monto Maximo Admitido.");
                                Console.WriteLine();
                            }
                            else
                            {
                                N1 = 1000;
                                N2 = 500;
                                N3 = 200;
                                N4 = 100;

                                N1 = imput / 1000;
                                if (N1 > 0)
                                {
                                    if (N1 >= 18)
                                    {
                                        if (imput <= 20000)
                                        {
                                            Console.WriteLine("Este Cajero Solo puede Ofrecer 18 Billetes de 1,000.");
                                            Console.WriteLine("Le Daremos Los 18 Billetes, mientras que el Resto del Dinero se lo daremos en el Cambio que tengamos Disponible.");
                                            Console.WriteLine("Disculpe los Inconvenientes");

                                            N11 = 18 * 1000;
                                            imput = imput - N11;

                                        }
                                        else
                                        {
                                            Console.WriteLine("Este Cajero Solo puede Ofrecer 18 Billetes de 1,000.");
                                            Console.WriteLine("Disculpe los Inconvenientes");
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + N1 + " Billetes de 1,000");
                                        Console.WriteLine();

                                        N11 = imput / 1000 * 1000;
                                        imput = imput - N11;
                                    }
                                }
                                

                                N2 = imput / 500;
                                if (N2 > 0)
                                {

                                    if (N2 >= 19)
                                    {
                                        Console.WriteLine("Este Cajero Solo puede Ofrecer 19 Billetes de 500.");
                                        Console.WriteLine("Disculpe los Inconvenientes");
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + N2 + " Billetes de 500");
                                        Console.WriteLine();

                                        N22 = imput / 500 * 500;
                                        imput = imput - N22;
                                    }
                                    
                                }


                                N3 = imput / 200;
                                if (N3 > 0)
                                {
                                    if (N3 >= 23)
                                    {
                                        Console.WriteLine("Este Cajero Solo puede Ofrecer 23 Billetes de 200.");
                                        Console.WriteLine("Disculpe los Inconvenientes");
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + N3 + " Billetes de 200");
                                        Console.WriteLine();

                                        N33 = imput / 200 * 200;
                                        imput = imput - N33;
                                    }
                                    
                                }


                                N4 = imput / 100;
                                if (N4 > 0)
                                {
                                    if (N4 >= 50)
                                    {
                                        Console.WriteLine("Este Cajero Solo puede Ofrecer 50 Billetes de 100.");
                                        Console.WriteLine("Disculpe los Inconvenientes");
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + N4 + " Billetes de 100");
                                        Console.WriteLine();
                                    }
                                    
                                }

                                Console.WriteLine("Gracias por utilizar el Cajero FDP Inversments");
                            }
                            
                            Console.WriteLine();

                            Console.ReadKey();
                            semaforo = true;
                            Console.ReadKey();
                            semaforo = false;

                            ; break;

                        case 2:

                            Console.WriteLine("FDP INVERSMENTS");
                            Console.WriteLine("Inserte el Monto Para Realizar la Transaccion. ");
                            int entry, L1, L2, L3, L4, L11, L22, L33;
                            entry = int.Parse(Console.ReadLine());
                            Console.WriteLine();


                            if (entry >= 10001)
                            {
                                Console.WriteLine("El Monto que ha Solicitado No Puede Ser Completado");
                            }
                            
                            else
                            {
                                if (entry == 10000)
                                {
                                    Console.WriteLine("Usted ha Solicitado el Monto Maximo Admitido.");
                                    Console.WriteLine();
                                }

                                L1 = 1000;
                                L2 = 500;
                                L3 = 200;
                                L4 = 100;

                                L1 = entry / 1000;
                                if (L1 > 0)
                                {
                                    if (L1 >= 18)
                                    {
                                            Console.WriteLine("Este Cajero Solo puede Ofrecer 18 Billetes de 1,000.");
                                            Console.WriteLine("Disculpe los Inconvenientes");
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + L1 + " Billetes de 1,000");
                                        Console.WriteLine();

                                        L11 = entry / 1000 * 1000;
                                        entry = entry - L11;
                                    }
                                }
                                

                                    L2 = entry / 500;
                                if (L2 > 0)
                                {

                                    if (L2 >= 19)
                                    {
                                        Console.WriteLine("Este Cajero Solo puede Ofrecer 19 Billetes de 500.");
                                        Console.WriteLine("Disculpe los Inconvenientes");
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + L2 + " Billetes de 500");
                                        Console.WriteLine();

                                        L22 = entry / 500 * 500;
                                        entry = entry - L22;
                                    }

                                }


                                L3 = entry / 200;
                                if (L3 > 0)
                                {
                                    if (L3 >= 23)
                                    {
                                        Console.WriteLine("Este Cajero Solo puede Ofrecer 23 Billetes de 200.");
                                        Console.WriteLine("Disculpe los Inconvenientes");
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + L3 + " Billetes de 200");
                                        Console.WriteLine();

                                        L33 = entry / 200 * 200;
                                        entry = entry - L33;
                                    }

                                }



                                L4 = entry / 100;
                                if (L4 > 0)
                                {
                                    if (L4 >= 50)
                                    {
                                        Console.WriteLine("Este Cajero Solo puede Ofrecer 50 Billetes de 100.");
                                        Console.WriteLine("Disculpe los Inconvenientes");
                                    }
                                    else
                                    {
                                        Console.WriteLine("El Monto es Compuesto por " + L4 + " Billetes de 100");
                                        Console.WriteLine();
                                    }

                                }

                                Console.WriteLine("Gracias por utilizar el Cajero FDP Inversments");
                            }
                            Console.ReadKey();
                            semaforo = true;
                            Console.ReadKey();
                            semaforo = false;

                            ; break;
       

                    }
                }

                else
                {

                    semaforo = true;
                }

                Console.Clear();

            } while (semaforo == false);
        }
    }
}